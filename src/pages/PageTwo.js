import React from 'react';
import axios from 'axios';
import url from '../config.js'
import { Table, Button, Containter, Modal, ModalBody, ModalHeader, FormGroup, ModalFooter } from 'reactstrap';
import ListContact from '../components/ListContact.js'

import ModalContact from '../components/ModalContact.js'

class PageTwo extends React.Component {

  constructor(props) {
    super(props);
    this.state = { 
      showModal : false,
      nombre : "",
      apellido : "",
      telefono : ""
    }
  }

  openModal() {
    this.setState({ showModal : true });
  }

  closeModal() {
    this.setState({ showModal : false });
  }

  crearContacto() {

    const nombre = document.getElementById("nombre").value;
    const apellido = document.getElementById("apellido").value;
    const telefono = document.getElementById("telefono").value;

    axios.post(`${url}/contactos`, { nombre, apellido, telefono })
      .then(res => {
        const contactos = res.data;
        this.setState({ contactos });
      })
    
    this.setState({ showModal : false })
  }

  render() {
    return(
      <React.Fragment>
        <Button color="success" onClick={ ()=>this.openModal() }>Insertar nuevo contacto</Button>
        <ListContact/>
        
        {/*
        <ModalContact
          show = {this.state.showModal}
        />
        */}

        <Modal isOpen={this.state.showModal} >
            <ModalHeader><h1>Crear contacto</h1></ModalHeader>
            <ModalBody>
              <FormGroup>
                <label>Nombre:</label>
                <input id="nombre" className="form-control" name="nombre" type="text"/>
              </FormGroup>
              <FormGroup>
                <label>Apellido:</label>
                <input id="apellido" className="form-control" name="apellido" type="text"/>
              </FormGroup>
              <FormGroup>
                <label>Telefono:</label>
                <input id="telefono" className="form-control" name="telefono" type="text"/>
              </FormGroup>
            </ModalBody>
            <ModalFooter>
                <Button color='primary' onClick={ ()=>this.crearContacto() }>Crear</Button>{' '}
                <Button color='secondary' onClick={ ()=>this.closeModal() } >Cancel</Button>
            </ModalFooter>
        </Modal>

      </React.Fragment>

    )    
  }

}

export default PageTwo;