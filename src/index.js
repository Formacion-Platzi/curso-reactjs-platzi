import React from 'react';
import ReactDOM from 'react-dom';
import Badge from "./components/Badge";

import App from './components/App.js'
import 'bootstrap/dist/css/bootstrap.min.css';

const container = document.getElementById('app');

/*
ReactDOM.render(
    <Badge 
        firstName="Javier" 
        lastName="Ramos"
        avatarUrl="https://gitlab.com/uploads/-/system/user/avatar/1827454/avatar.png"
        jobTitle="Frontend Engineer" 
        twitter="sparragus"
    />, 
    container
);
*/

ReactDOM.render(<App/>, container);
