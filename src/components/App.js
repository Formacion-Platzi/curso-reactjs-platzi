import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import PageOne from '../pages/PageOne.js';
import PageTwo from '../pages/PageTwo.js';
import NotFound from '..//pages/NotFound.js';

import Layout from './Layout.js';

function App() {
    return (
        <BrowserRouter>
            <Layout>
                <Switch>
                    <Route exact path="/page/one" component={PageOne} />
                    <Route exact path="/page/two" component={PageTwo} />
                    <Route component="NotFound" />
                </Switch>
            </Layout>
        </BrowserRouter>
    );
}

export default App;